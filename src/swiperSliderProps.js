import SwiperCore, { Autoplay, Navigation, Pagination } from "swiper";
SwiperCore.use([Pagination, Navigation, Autoplay]);

export const activitySlider = {
  slidesPerView: 1,
  spaceBetween: 30,
  autoplay: true,
  // loop: true,
  pagination: false,
  navigation: {
    prevEl: ".my_prev2",
    nextEl: ".my_next2",
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    640: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    1024: {
      slidesPerView: 3,
      spaceBetween: 40,
    },
  },
};

export const portfolioSlider = {
  slidesPerView: 3,
  spaceBetween: 30,
  // loop: true,
  pagination: false,
  navigation: {
    prevEl: ".my_prev",
    nextEl: ".my_next",
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    640: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    1024: {
      slidesPerView: 3,
      spaceBetween: 40,
    },
  },
};

export const newsSlider = {
  slidesPerView: 3,
  spaceBetween: 30,
  // loop: true,
  pagination: false,
  navigation: {
    prevEl: ".my_prev",
    nextEl: ".my_next",
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    640: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    1024: {
      slidesPerView: 3,
      spaceBetween: 40,
    },
  },
};
