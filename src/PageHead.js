import Head from "next/head";
const PageHead = ({ page }) => {
  return (
    <Head>
      <title>Thailand National Day RTCG Chengdu 2021</title>
      <link rel="icon" href="/img/favicon.ico" />
    </Head>
  );
};

export default PageHead;
