const VideoBox = ({ url, style }) => {
    return (
      <>

          <video
            src={url}
            controls={true}
            style={style}
          />

      </>
    );
  };

export default VideoBox