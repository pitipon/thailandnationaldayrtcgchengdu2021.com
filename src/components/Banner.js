import { useState } from "react";
import Image from 'next/image'
import Link from 'next/link';

const Banner = () => {
  // style="background-image: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), );"
  return (

        <div className='banner-container'>
          <div className='banner-container-button'>
            <a href='/home'>
              <button type="button" className="btn btn-warning btn-lg">Enter</button>
            </a>
          </div>
          <div style={{width:'100vw', height:'55vw', marginTop: '60px'}}>
          <Image
            src="/img/banner.jpg"
            alt="Picture of the author"
            layout='fill'
            objectFit='contain'
          />
          </div>
          {/* <h1 style={{color: '#D0A837'}}>Celebrating</h1>
          <p style={{color: 'grey'}}>Thailand's Natiional Day, Father's Day, and the Birthday Anniversary of King Bhumibol Adulyadej The Great</p> */}
          <div style={{height:'3vw'}}></div>

        </div>

  );
};

export default Banner;
