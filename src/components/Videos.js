import React, { Fragment, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { newsSlider } from "../swiperSliderProps";
import { VideoModal } from "./VideoModal";

const Videos = () => {
  const [open, setOpen] = useState(false);
  const [modalValue, setModalValue] = useState({
    video: null,
    title: "",
    date: "",
  });
  const onOpenModal = (video, title, date) => {
    setOpen(true);
    setModalValue({ video, title, date });
  };
  const onCloseModal = () => {
    setOpen(false);
    setModalValue({ video: null, title: "", date: "" });
  };

  return (
    <Fragment>
      <div className="kura_tm_section" id="videos">
        <div className="kura_tm_news">
          <div className="container">
            <div className="kura_tm_main_title">
              <span>Resilient and vibrant Thailand</span>
              <h3>充满活力的泰国</h3>
            </div>
            <div className="news_list wow fadeInUp" data-wow-duration=".7s">
              <div className="slick-container">
                <div className="slick-wrapper">
                  <Swiper {...newsSlider}>
                    <SwiperSlide key='video-1' className="slick-slide">
                      <div className="list_inner">
                        <div className="image">
                          <img src="/img/videos/410-460.jpg" alt="" />
                          <div
                            className="main"
                            style={{
                              backgroundImage: "url(/img/videos/1.png)",
                            }}
                          ></div>
                        </div>
                        <div className="overlay"></div>
                        <img
                          className="svg"
                          src="/img/svg/right-arrow.svg"
                          alt=""
                        />
                        <div key='' className="details">
                          <span>Video</span>
                          <h3>A LEGACY FOR THE PEOPLE</h3>
                        </div>
                        <a
                          className="kura_tm_full_link"
                          onClick={() =>
                            onOpenModal(
                              `https://image.mfa.go.th/mfa/0/wdW3FTtVMc/5_DEC_2021_Thailand_Day/20211014_Moradokchart_ENG_50M-new.mp4`,
                              "A LEGACY FOR THE PEOPLE",
                              "Video"
                            )
                          }
                        ></a>

                      </div>
                    </SwiperSlide>
                    <SwiperSlide key='video-2' className="slick-slide">
                      <div className="list_inner">
                        <div className="image">
                          <img src="/img/videos/410-460.jpg" alt="" />
                          <div
                            className="main"
                            style={{
                              backgroundImage: "url(/img/videos/2.png)",
                            }}
                          ></div>
                        </div>
                        <div className="overlay"></div>
                        <img
                          className="svg"
                          src="/img/svg/right-arrow.svg"
                          alt=""
                        />
                        <div className="details">
                          <span>Video</span>
                          <h3>AMAZING NEW CHAPTERS</h3>
                        </div>
                        <a
                          className="kura_tm_full_link"
                          onClick={() =>
                            onOpenModal(
                              `https://image.mfa.go.th/mfa/0/wdW3FTtVMc/5_DEC_2021_Thailand_Day/tat_50M-new.mp4`,
                              "AMAZING NEW CHAPTERS",
                              "Video"
                            )
                          }
                        ></a>

                      </div>
                    </SwiperSlide>
                    <SwiperSlide key='video-3' className="slick-slide">
                      <div className="list_inner">
                        <div className="image">
                          <img src="/img/portfolio/410-460.jpg" alt="" />
                          <div
                            className="main"
                            style={{
                              backgroundImage: "url(/img/videos/3.png)",
                            }}
                          ></div>
                        </div>
                        <div className="overlay"></div>
                        <img
                          className="svg"
                          src="/img/svg/right-arrow.svg"
                          alt=""
                        />
                        <div className="details">
                          <span>Video</span>
                          <h3>THE LANDMARKS OF EEC</h3>
                        </div>
                        <a
                          className="kura_tm_full_link"
                          onClick={() =>
                            onOpenModal(
                              `https://image.mfa.go.th/mfa/0/wdW3FTtVMc/5_DEC_2021_Thailand_Day/VDO_Landmark_EEC_(CN_Sub)-new.mp4`,
                              "THE LANDMARKS OF EEC",
                              "Video"
                            )
                          }
                        ></a>

                      </div>
                    </SwiperSlide>

                  </Swiper>
                </div>

                <div className="kura_tm_swiper_progress fill">
                  <div className="my_pagination_in"></div>
                  <div className="my_navigation">
                    <ul>
                      <li>
                        <a className="my_prev">
                          <img
                            className="svg"
                            src="img/svg/right-arrow.svg"
                            alt=""
                          />
                        </a>
                      </li>
                      <li>
                        <a className="my_next">
                          <img
                            className="svg"
                            src="img/svg/right-arrow.svg"
                            alt=""
                          />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <VideoModal
        open={open}
        onCloseModal={() => onCloseModal()}
        video={modalValue.video}
        title={modalValue.title}
        date={modalValue.date}
      />
    </Fragment>
  );
};

export default Videos;
