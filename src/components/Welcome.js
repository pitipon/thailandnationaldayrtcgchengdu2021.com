import { useState } from "react";
import { HomeModal } from "./ContentModal";

const Home = () => {
  const [open, setOpen] = useState(false);
  const [modalValue, setModalValue] = useState({
    img: null,
    title: "",
  });
  const onOpenModal = (img, title) => {
    setOpen(true);
    setModalValue({ img, title });
  };
  const onCloseModal = () => {
    setOpen(false);
    setModalValue({ img: null, title: "" });
  };
  return (
    <div className="kura_tm_section" id="welcome">
      <div className="kura_tm_hero" style={{background:'white'}}>
        <div className="container">
          <div className="content" style={{justifyContent: 'center', minHeight:'20vh'}}>

            <img className="image" src="/img/welcome.jpg" alt="" />
            <div className="down anchor" >
              <a href="#video">
                <img className="svg" src="/img/svg/down-arrow.svg" alt="" />
              </a>
            </div>
          </div>
        </div>
        <div style={{paddingBottom:'5vw'}}></div>
      </div>

    </div>
  );
};

export default Home;
