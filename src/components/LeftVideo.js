import { Player, PosterImage, BigPlayButton } from 'video-react';

const LeftVideo = () => {

  return (
    <div className="kura_tm_section" id="home">
      <div className="kura_tm_hero" style={{paddingTop:'0', background:'#FCF2CA'}}>
        <div className="container">
          <div className="content">
            <div className="left2">


              <Player
                poster="/img/activity/1.jpg"
                src="https://image.mfa.go.th/mfa/0/wdW3FTtVMc/Test/EEC.mp4"
              >
                <BigPlayButton position="center" />
              </Player>

            </div>
            <div className="right2" >
              <span className="name" style={{fontSize: '25px'}}> Message by H.E. General Prayut Chan-o-cha (Ret.) Prime Minister of the Kingdom of Thailand </span>
              <h3 className="job" style={{fontSize: '25px'}}>泰国总理巴育•占奥差上将致辞</h3>
            </div>

          </div>
        </div>
      </div>

    </div>
  );
};

export default LeftVideo;
