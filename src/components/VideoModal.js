import { Modal } from "react-responsive-modal";
import VideoBox from "./VideoBox";

export const VideoModal = ({ open, onCloseModal, video, title, date }) => {
  console.log('video', video)
  return (
    <Modal open={open} onClose={() => onCloseModal()} center>
      <div className="box_inner">
        <div className="description_wrap" style={{ padding: "30px" }}>
          <div className="news_popup_informations">

            <br />
            <div className="details" style={{ marginBottom: "20px" }}>
              {date && <span>{date}</span>}
              <h3>{title ? title : ``}</h3>
              <div></div>
            </div>

            <VideoBox
              url={video}
              style={{width:'100%'}}
            />
          </div>
        </div>
      </div>
    </Modal>
  );
};
