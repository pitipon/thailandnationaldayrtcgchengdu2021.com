import React from "react";

const Qrcode = () => {
  return (
    <div className="kura_tm_section" id="quiz">
      <div className="kura_tm_contact">
        <div className="container">
          <div className="kura_tm_main_title">
            <span>Quiz challenge</span>
            <h3>有奖问答有奖问答</h3>
          </div>
          <div className="contact_inner">
            <div className="left wow fadeInUp" data-wow-duration=".7s">
              <div className="text">
                <p>
                Play quiz and get free voucher!
                </p>
                <div style={{textAlign:'center'}}>
                  <img src='/img/qrcode.jpeg'/>
                </div>
                <a href='https://ks.wjx.top/vj/QWIacb4.aspx'>
                  <button type="button" className="btn btn-warning btn-lg" style={{width:'100%'}}>Enter</button>
                </a>
              </div>

            </div>

            <div className="right">
                <img src='/img/quiz.jpeg'></img>
            </div>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Qrcode;
