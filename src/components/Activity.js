import React, { Fragment, useState } from "react";
import LightBox from "react-awesome-lightbox";
import { Swiper, SwiperSlide } from "swiper/react";
import { activitySlider } from "../swiperSliderProps";

const IMAGES = [
  {
    src: "/img/activity/a.jpg",
    thumbnail: "/img/activity/a.jpg",
    title: "สารจากอธิบดีสำนักงานการต่างประเทศมณฑลเสฉวนและนครฉงชิ่ง แสดงความยินดีเนื่องในโอกาสวันชาติไทย 5 ธันวาคม 2564",
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%AA%E0%B8%B2%E0%B8%A3%E0%B8%88%E0%B8%B2%E0%B8%81%E0%B8%AD%E0%B8%98%E0%B8%B4%E0%B8%9A%E0%B8%94%E0%B8%B5%E0%B8%AA%E0%B8%B3%E0%B8%99%E0%B8%B1%E0%B8%81%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B9%88%E0%B8%B2%E0%B8%87%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%97%E0%B8%A8%E0%B8%A1%E0%B8%93%E0%B8%91%E0%B8%A5%E0%B9%80%E0%B8%AA%E0%B8%89%E0%B8%A7%E0%B8%99%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B8%89%E0%B8%87?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/c.jpg",
    thumbnail: "/img/activity/c.jpg",
    title: "กงสุลใหญ่ ณ นครเฉิงตู เข้าพบหารือเลขาธิการพรรคคอมมิวนิสต์จีน ประจำเมืองตูเจียงเยี่ยน มณฑลเสฉวน และเยี่ยมชมบริษัท Sichuan Zhengpan Agricultural Development Co. Ltd. ซึ่งร่วมกับเครือเจริญโภคภัณฑ์",
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9-%E0%B9%80%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%9E%E0%B8%9A%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B8%B7%E0%B8%AD%E0%B9%80%E0%B8%A5%E0%B8%82%E0%B8%B2%E0%B8%98%E0%B8%B4%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%9E%E0%B8%A3%E0%B8%A3%E0%B8%84%E0%B8%84%E0%B8%AD%E0%B8%A1%E0%B8%A1?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/b.jpg",
    thumbnail: "/img/activity/b.jpg",
    title: "พิธีทางศาสนาและโครงการจิตอาสา เนื่องในวันคล้ายวันพระบรมราชสมภพพระบาทสมเด็จพระบรมชนกาธิเบศร มหาภูมิพลอดุลยเดชมหาราช บรมนาถบพิตร วันชาติ และวันพ่อแห่งชาติ 5 ธันวาคม 2564",
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%9E%E0%B8%B4%E0%B8%98%E0%B8%B5%E0%B8%97%E0%B8%B2%E0%B8%87%E0%B8%A8%E0%B8%B2%E0%B8%AA%E0%B8%99%E0%B8%B2%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B9%82%E0%B8%84%E0%B8%A3%E0%B8%87%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%88%E0%B8%B4%E0%B8%95%E0%B8%AD%E0%B8%B2%E0%B8%AA%E0%B8%B2-%E0%B9%80%E0%B8%99%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%83%E0%B8%99%E0%B8%A7%E0%B8%B1%E0%B8%99%E0%B8%84%E0%B8%A5%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%A7%E0%B8%B1%E0%B8%99%E0%B8%9E?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/1.jpg",
    thumbnail: "/img/activity/1.jpg",
    title: "เมื่อวันที่ 27 กันยายน 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วย รองกงสุลใหญ่ ข้าราชการและเจ้าหน้าที่สถานกงสุลใหญ่ฯ ได้เยือนโรงเรียนประถมศึกษาเหมียนหยางเซียนเฟิงลู่ สิรินธร เมืองเหมียนหยาง มณฑลเสฉวน",
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B8%97%E0%B8%B4%E0%B8%9E%E0%B8%A2%E0%B9%8C%E0%B8%A7%E0%B8%A3%E0%B8%A3%E0%B8%93-%E0%B8%A8%E0%B8%B8%E0%B8%A0%E0%B8%A1%E0%B8%B4%E0%B8%95%E0%B8%A3%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%88%E0%B8%B2-%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9%E0%B8%87?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/2.jpg",
    thumbnail: "/img/activity/2.jpg",
    title: `เมื่อวันที่ 9 - 10 กันยายน 2564 สถานกงสุลใหญ่ ณ นครเฉิงตู ร่วมกับมหาวิทยาลัยเฉิงตู จัดกิจกรรมทางวิชาการในหัวข้อ "Connecting Thailand's BCG Model with China's Smart City Development towards Sustainable Development” ให้แก่นักศึกษาไทยและนักศึกษาจีนเมื่อวันที่ 9 - 10 กันยายน 2564 สถานกงสุลใหญ่ ณ นครเฉิงตู ร่วมกับมหาวิทยาลัยเฉิงตู จัดกิจกรรมทางวิชาการในหัวข้อ "Connecting Thailand's BCG Model with China's Smart City Development towards Sustainable Development” ให้แก่นักศึกษาไทยและนักศึกษาจีน`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%81%E0%B8%A3%E0%B8%A3%E0%B8%A1%E0%B8%97%E0%B8%B2%E0%B8%87%E0%B8%A7%E0%B8%B4%E0%B8%8A%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A3-connecting-thailand-s-bcg-model?cate=5d81af8215e39c2eb4004ac9'
  },

  {
    src: "/img/activity/3.jpg",
    thumbnail: "/img/activity/3.jpg",
    title: `ระหว่างวันที่ 9-11 กันยายน 2564 สถานกงสุลใหญ่ ณ นครเฉิงตูได้เปิดคูหาจัดแสดงสินค้าและผลิตภัณฑ์ตัวอย่างของผู้ประกอบการ SMEs ไทยในงาน The 1st Western China Cross-Border E-Commerce Expo `,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%AA%E0%B8%81%E0%B8%8D-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9%E0%B9%80%E0%B8%9B%E0%B8%B4%E0%B8%94%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94%E0%B8%9E%E0%B8%B2%E0%B8%93%E0%B8%B4%E0%B8%8A%E0%B8%A2%E0%B9%8C%E0%B8%AD%E0%B8%B4%E0%B9%80%E0%B8%A5%E0%B9%87%E0%B8%81%E0%B8%97%E0%B8%A3%E0%B8%AD%E0%B8%99%E0%B8%B4%E0%B8%81%E0%B8%AA%E0%B9%8C%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%A1%E0%B8%9E%E0%B8%A3?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/4.jpg",
    thumbnail: "/img/activity/4.jpg",
    title: ` เมื่อวันที่ 2-4 กันยายน 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วยนายวิธวินห์ โตเกียรติรุ่งเรือง กงสุล ได้ร่วมงาน 8th Sichuan International Travel Expo ณ เมืองเล่อซาน มณฑลเสฉวน`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/8th-sichuan-international-travel-expo-%E0%B8%93-%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%A5%E0%B9%88%E0%B8%AD%E0%B8%8B?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/5.jpg",
    thumbnail: "/img/activity/5.jpg",
    title: `เมื่อวันที่ 27 สิงหาคม 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู ได้ร่วมงาน Amazing Thailand Night ณ La Cardiere Garden City นครเฉิงตู ซึ่งจัดโดยสำนักงาน ททท. นครเฉิงตู`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%87%E0%B8%B2%E0%B8%99-amazing-thailand-night-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9-%E0%B8%A1%E0%B8%93%E0%B8%91%E0%B8%A5%E0%B9%80%E0%B8%AA%E0%B8%89%E0%B8%A7%E0%B8%99?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/6.jpg",
    thumbnail: "/img/activity/6.jpg",
    title: `เมื่อวันที่ 26 สิงหาคม 2564 สถานกงสุลใหญ่ ณ นครเฉิงตู นำโดยนางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ พร้อมด้วยข้าราชการ ผู้แทนหน่วยงานทีมประเทศไทย เจ้าหน้าที่ท้องถิ่น ตลอดจนผู้แทนภาคเอกชนไทยและนักศึกษาไทยในมณฑลเสฉวนได้จัดกิจกรรมจิตอาสา “เราทำความดี ด้วยหัวใจ” เนื่องในโอกาสวันเฉลิมพระชนมพรรษา สมเด็จพระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ พระบรมราชชนนีพันปีหลวง วันที่ 12 สิงหาคม 2564 ณ โรงเรียนประถมอู่อีชุนจงซิน (五一村中心小学) อำเภอเฉียนเหวย เมืองเล่อซาน มณฑลเสฉวน โดยได้นำสิ่งของที่จำเป็นมอบให้แก่นักเรียน เช่น ชุดเครื่องเขียน รองเท้า สมุด ดินสอ กล่องดินสอ ข้าวสาร`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/20210826-%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%81%E0%B8%A3%E0%B8%A3%E0%B8%A1%E0%B8%88%E0%B8%B4%E0%B8%95%E0%B8%AD%E0%B8%B2%E0%B8%AA%E0%B8%B2?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/7.png",
    thumbnail: "/img/activity/7.png",
    title: `เมื่อวันที่ 26 กรกฎาคม 2564 สถานกงสุลใหญ่ ณ นครเฉิงตู นำโดย นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ พร้อมด้วยข้าราชการ และเจ้าหน้าที่สถานกงสุลใหญ่ฯ และหน่วยงานทีมประเทศไทย ตลอดจนชาวไทยในมณฑลเสฉวน ได้จัดกิจกรรมทำบุญและบริจาคสิ่งของเนื่องในวันอาสาฬหบูชาและวันเข้าพรรษา ณ วัดปี่ซาน เขตต้าอี้ (壁山寺) นครเฉิงตู มณฑลเสฉวน กิจกรรมประกอบด้วย (1) คณะสงฆ์สวดมนต์ตามประเพณีจีน (2) การสวดมนต์บูชาพระรัตนตรัยแบบไทย และ (3) การถวายข้าวสาร อาหารแห้ง น้ำมัน น้ำดื่ม และของใช้ที่จำเป็น แด่พระอาจารย์​จี่ฮุย เจ้าอาวาส และคณะ`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%81%E0%B8%A3%E0%B8%A3%E0%B8%A1%E0%B8%97%E0%B8%B3%E0%B8%9A%E0%B8%B8%E0%B8%8D%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%88%E0%B8%B2%E0%B8%84%E0%B8%AA%E0%B8%B4%E0%B9%88%E0%B8%87%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%99%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%83%E0%B8%99%E0%B8%A7%E0%B8%B1%E0%B8%99%E0%B8%AD%E0%B8%B2%E0%B8%AA%E0%B8%B2%E0%B8%AC%E0%B8%AB%E0%B8%9A%E0%B8%B9%E0%B8%8A%E0%B8%B2%E0%B9%81?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/8.jpg",
    thumbnail: "/img/activity/8.jpg",
    title: `เมื่อวันที่ 8 กรกฎาคม 2564 สถานกงสุลใหญ่ ณ นครเฉิงตูร่วมกับคณะกรรมการพาณิชย์นครฉงชิ่ง (Chongqing Municipal Commission of Commerce) จัดงานสัมมนา “Investment in Thailand : Your Opportunities and Incentives” ณ นครฉงชิ่ง`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%AA%E0%B8%B1%E0%B8%A1%E0%B8%A1%E0%B8%99%E0%B8%B2%E0%B8%AA%E0%B9%88%E0%B8%87%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B4%E0%B8%A1%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%A5%E0%B8%87%E0%B8%97%E0%B8%B8%E0%B8%99%E0%B9%83%E0%B8%99%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%97%E0%B8%A8%E0%B9%84%E0%B8%97%E0%B8%A2-%E0%B9%80%E0%B8%9E%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B9%80%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%A2%E0%B8%A1%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%9E?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/9.jpg",
    thumbnail: "/img/activity/9.jpg",
    title: `เมื่อวันที่ 4 มิถุนายน 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู ได้เข้าร่วมพิธีเปิดงาน Thai Fruits Golden Months Chengdu 2021 ณ โซนผลไม้นำเข้าจากอาเซียน ศูนย์การค้าสินค้าเกษตรนานาชาติมณฑลเสฉวน`,
    sub: "activity",
    link: 'https://www.mfa.go.th/th/content/%E0%B8%87%E0%B8%B2%E0%B8%99-thai-fruits-golden-months-chengdu-2021-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89'
  },
  {
    src: "/img/activity/10.jpg",
    thumbnail: "/img/activity/10.jpg",
    title: `เมื่อวันที่ 14 พฤษภาคม 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู ได้เข้าร่วมพิธีเปิดงาน Thai Fruits Golden Months Chongqing 2021 ณ ห้างสรรพสินค้าหย่งฮุย นครฉงชิ่ง ซึ่งสำนักงานส่งเสริมการค้าในต่างประเทศ ณ นครเฉิงตูร่วมกับห้างสรรพสินค้าหย่งฮุยและบริษัท JOY WING MAO GROUP จัดขึัน เพื่อส่งเสริมให้ชาวจีน`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/thai-fruits-golden-months-chongqing-2021?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/11.jpg",
    thumbnail: "/img/activity/11.jpg",
    title: `เมื่อวันที่ 26 - 28 เมษายน 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู และคณะ ได้เยือนเมืองอี๋ปิน มณฑลเสฉวน`,
    sub: "activity",
    link: 'https://mfa.go.th/th/content/%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B8%97%E0%B8%B4%E0%B8%9E%E0%B8%A2%E0%B9%8C%E0%B8%A7%E0%B8%A3%E0%B8%A3%E0%B8%93-%E0%B8%A8%E0%B8%B8%E0%B8%A0%E0%B8%A1%E0%B8%B4%E0%B8%95%E0%B8%A3%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%88%E0%B8%B2-%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9?cate=5d5bcb4e15e39c3060006843'
  },
  {
    src: "/img/activity/12.jpg",
    thumbnail: "/img/activity/12.jpg",
    title: `เมื่อวันที่ 22-23 เมษายน 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู ได้เยือนนครฉงชิ่ง`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9-%E0%B9%80%E0%B8%A2%E0%B8%B7%E0%B8%AD%E0%B8%99%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B8%89%E0%B8%87%E0%B8%8A%E0%B8%B4%E0%B9%88%E0%B8%87-%E0%B9%80%E0%B8%9E%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%A3%E0%B9%88%E0%B8%A7%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B8%B7?cate=60373fa40f8b032234650132'
  },
  {
    src: "/img/activity/13.jpg",
    thumbnail: "/img/activity/13.jpg",
    title: `ระหว่างวันที่ 10-11 เมษายน 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู ได้เข้าร่วมงาน Chongqing Foreign Consulates Spring Games ซึ่งสำนักงานการต่างประเทศ นครฉงชิ่งและเขตปี้ซานจัดขึ้น ณ เขตปี้ซาน นครฉงชิ่ง`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B8%97%E0%B8%B4%E0%B8%9E%E0%B8%A2%E0%B9%8C%E0%B8%A7%E0%B8%A3%E0%B8%A3%E0%B8%93-%E0%B8%A8%E0%B8%B8%E0%B8%A0%E0%B8%A1%E0%B8%B4%E0%B8%95%E0%B8%A3%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%88%E0%B8%B2-%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9-%E0%B8%A3%E0%B9%88?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/14.jpg",
    thumbnail: "/img/activity/14.jpg",
    title: `เมื่อวันที่ 2 เมษายน 2564 สถานกงสุลใหญ่ ณ นครเฉิงตู และสำนักงาน ททท. ประจำนครเฉิงตู ได้เข้าร่วมกิจกรรมรณรงค์สร้างความตระหนักรู้ เนื่องในโอกาสวันออทิสติกโลก ซึ่งจัดโดยสมาคมผู้พิการของนครเฉิงตูและ Haichang Ocean Park`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%AA%E0%B8%96%E0%B8%B2%E0%B8%99%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9-%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B8%AA%E0%B8%B3%E0%B8%99%E0%B8%B1%E0%B8%81%E0%B8%87%E0%B8%B2%E0%B8%99-%E0%B8%97%E0%B8%97%E0%B8%97-%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%88%E0%B8%B3%E0%B8%99%E0%B8%84%E0%B8%A3?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/15.jpg",
    thumbnail: "/img/activity/15.jpg",
    title: `เมื่อวันที่ 26-27 มีนาคม 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู และนางกนกกิตติกา กฤตย์วุฒิกร ผู้อำนวยการการ ททท. สำนักงานนครเฉิงตู ได้เข้าร่วมงาน Mending Tea Cultural & Tourism Festival ครั้งที่ 17 ณ เมืองหย่าอัน มณฑลเสฉวน`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B8%97%E0%B8%B4%E0%B8%9E%E0%B8%A2%E0%B9%8C%E0%B8%A7%E0%B8%A3%E0%B8%A3%E0%B8%93-%E0%B8%A8%E0%B8%B8%E0%B8%A0%E0%B8%A1%E0%B8%B4%E0%B8%95%E0%B8%A3%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%88%E0%B8%B2-%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9-%E0%B9%81%E0%B8%A5?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/16.jpg",
    thumbnail: "/img/activity/16.jpg",
    title: `เมื่อวันที่ 25 - 26 มีนาคม 2564 สถานกงสุลใหญ่ ณ นครเฉิงตู ได้จัดโครงการเสริมสร้างเส้นทางอาชีพสู่การเป็นผู้ประกอบการรุ่นใหม่ให้แก่นักศึกษาไทยในมณฑลเสฉวนและนครฉงชิ่ง`,
    sub: "activity",
    link: 'https://www.mfa.go.th/th/content/%E0%B8%AA%E0%B8%96%E0%B8%B2%E0%B8%99%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9-%E0%B8%88%E0%B8%B1%E0%B8%94%E0%B9%82%E0%B8%84%E0%B8%A3%E0%B8%87%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B4%E0%B8%A1%E0%B8%AA%E0%B8%A3%E0%B9%89%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%AA%E0%B9%89%E0%B8%99?cate=5d5bcb4e15e39c3060006843https://www.mfa.go.th/th/content/%E0%B8%AA%E0%B8%96%E0%B8%B2%E0%B8%99%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9-%E0%B8%88%E0%B8%B1%E0%B8%94%E0%B9%82%E0%B8%84%E0%B8%A3%E0%B8%87%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B4%E0%B8%A1%E0%B8%AA%E0%B8%A3%E0%B9%89%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%AA%E0%B9%89%E0%B8%99?cate=5d5bcb4e15e39c3060006843'
  },
  {
    src: "/img/activity/17.jpg",
    thumbnail: "/img/activity/17.jpg",
    title: `เมื่อวันที่ 20 มีนาคม 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตูพร้อมด้วย ผอ. สคต. ผอ. ททท. และข้าราชการ ได้ร่วมงานฉลองการได้รับมอบใบรับรองตราสัญลักษณ์ "Thai Select" ให้แก่ร้านอาหารไท่เซียงหมี่จำนวน 5 สาขา ณ โรงแรม Waldorf Astoria นครเฉิงตู`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/thai-select-20-%E0%B8%A1%E0%B8%B5%E0%B8%99%E0%B8%B2%E0%B8%84%E0%B8%A1-2564?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/18b.jpg",
    thumbnail: "/img/activity/18b.jpg",
    title: `เมื่อวันที่ 5 มีนาคม 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วยนางสาวพิชชาภรณ์ หลิวเจริญ กงสุล และเจ้าหน้าที่สถานกงสุลใหญ่ฯ ได้เยี่ยมชมวิทยาลัยอาชีวศึกษานครเฉิงตูตามคำเชิญ และพบหารือกับนายหยู จื้อผิง เลขาธิการพรรคฯ วิทยาลัยอาชีวศึกษานครเฉิงตู นายตู้ ไห่เฉิง รองอธิการบดี และผู้บริหารระดับสูง เกี่ยวกับความร่วมมือด้านการศึกษาระหว่างวิทยาลัยฯ กับมหาวิทยาลัยของไทย`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B8%97%E0%B8%B4%E0%B8%9E%E0%B8%A2%E0%B9%8C%E0%B8%A7%E0%B8%A3%E0%B8%A3%E0%B8%93-3?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/19.jpg",
    thumbnail: "/img/activity/19.jpg",
    title: `เมื่อวันที่ 26 กุมภาพันธ์ 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วย นายวรันณ์ธร คชทโรภาส กงสุล ได้พบและหารือกับนายหวัง ชิงหยวน อธิการบดี และผู้บริหารระดับสูงของมหาวิทยาลัยเฉิงตู เพื่อส่งเสริมความร่วมมือที่ใกล้ชิดมากขึ้นระหว่างมหาวิทยาลัยฯ กับไทยในด้านการศึกษา วิชาการ การวิจัย การแลกเปลี่ยนอาจารย์และนักศึกษา รวมถึงการเรียน การสอนภาษาไทย-จีน และการให้ทุนการศึกษาแก่นักศึกษาไทย`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B8%97%E0%B8%B4%E0%B8%9E%E0%B8%A2%E0%B9%8C%E0%B8%A7%E0%B8%A3%E0%B8%A3%E0%B8%93%E0%B8%AF-%E0%B9%80%E0%B8%A2%E0%B8%B7%E0%B8%AD%E0%B8%99%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%A7%E0%B8%B4%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%A5%E0%B8%B1%E0%B8%A2%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/20.jpg",
    thumbnail: "/img/activity/20.jpg",
    title: `เมื่อวันที่ 5 กุมภาพันธ์ 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วยนางสาวพิชชาภรณ์ หลิวเจริญ กงสุล และเจ้าหน้าที่สถานกงสุลใหญ่ฯ ได้เดินทางไปศึกษาดูงาน ณ ศูนย์นวัตกรรมการแพทย์ซานอี (Tri-Meds Innovation Center) Chengdu Medical City เขตเวินเจียง นครเฉิงตู`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B8%97%E0%B8%B4%E0%B8%9E%E0%B8%A2%E0%B9%8C%E0%B8%A7%E0%B8%A3%E0%B8%A3%E0%B8%93-2?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/21.jpg",
    thumbnail: "/img/activity/21.jpg",
    title: `เมื่อวันที่ 7 ม.ค. 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วยข้าราชการและเจ้าหน้าที่สถานกงสุลใหญ่ฯ ได้เยี่ยมชมมหาวิทยาลัย​วัฒนธรรม​และศิลปะ​แห่งมณฑล​เสฉวน​ (Sichuan University of Culture and Arts)  `,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/sichuan-university-of-culture-and-arts?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/22.jpg",
    thumbnail: "/img/activity/22.jpg",
    title: `เมื่อวันที่ 8 มกราคม 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วยนางสาวเนตรนภิศ จุลกนิษฐ ผู้อำนวยการสำนักงานส่งเสริมการค้าในต่างประเทศ ณ นครเฉิงตู นางสาวพิชชาภรณ์ หลิวเจริญ กงสุล และเจ้าหน้าที่สถานกงสุลใหญ่ฯ ได้เข้าพบกับนางถัง เอี้ยน รองอธิบดีกรมพาณิชย์มณฑลเสฉวน ณ ที่ทำการกรมพาณิชย์มณฑลเสฉวน`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/3-%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B8%97%E0%B8%B4%E0%B8%9E%E0%B8%A2%E0%B9%8C%E0%B8%A7%E0%B8%A3%E0%B8%A3%E0%B8%93-%E0%B8%A8%E0%B8%B8%E0%B8%A0%E0%B8%A1%E0%B8%B4%E0%B8%95%E0%B8%A3%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%88%E0%B8%B2-%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%A5%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88-%E0%B8%93-%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%80%E0%B8%89%E0%B8%B4%E0%B8%87%E0%B8%95%E0%B8%B9?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/23.jpg",
    thumbnail: "/img/activity/23.jpg",
    title: `เมื่อวันที่ 7 มกราคม 2564 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วยข้าราชการและเจ้าหน้าที่​สถาน​กงสุลใหญ่​ฯ​ ได้เดินทางไปเยี่ยมโรงเรียนประถมศึกษาเหมียนหยางเซียนเฟิงลู่ สิรินธร `,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/%E0%B9%82%E0%B8%A3%E0%B8%87%E0%B9%80%E0%B8%A3%E0%B8%B5%E0%B8%A2%E0%B8%99%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%96%E0%B8%A1%E0%B8%A8%E0%B8%B6%E0%B8%81%E0%B8%A9%E0%B8%B2%E0%B9%80%E0%B8%AB%E0%B8%A1%E0%B8%B5%E0%B8%A2%E0%B8%99%E0%B8%AB%E0%B8%A2%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%8B%E0%B8%B5%E0%B8%A2%E0%B8%99%E0%B9%80%E0%B8%9F%E0%B8%B4%E0%B8%87%E0%B8%A5%E0%B8%B9%E0%B9%88-%E0%B8%AA%E0%B8%B4%E0%B8%A3%E0%B8%B4%E0%B8%99%E0%B8%98%E0%B8%A3?cate=5d81af8215e39c2eb4004ac9'
  },
  {
    src: "/img/activity/24.png",
    thumbnail: "/img/activity/24.png",
    title: ` เมื่อวันที่ 22 ธันวาคม 2563 นางทิพย์วรรณ ศุภมิตรกิจจา กงสุลใหญ่ ณ นครเฉิงตู พร้อมด้วยนางสาวเนตรนภิศ จุลกนิษฐ ผู้อำนวยการสำนักงานส่งเสริมการค้าในต่างประเทศ ณ นครเฉิงตู นางสาวพิชชาภรณ์ หลิวเจริญ กงสุล และเจ้าหน้าที่สถานกงสุลใหญ่ฯ ได้ศึกษาดูงานที่ Chengdu International Railway Port ณ เขตชิงไป๋เจียง นครเฉิงตู`,
    sub: "activity",
    link: 'https://chengdu.thaiembassy.org/th/content/chengdu-international-railway-port?cate=5d81af8215e39c2eb4004ac9'
  }
];

const Activity = () => {
  const [showLight, setShowLight] = useState(null);

  const showLightBox = (index) => {
    setShowLight({
      startIndex: index,
    });
  };

  const hideLightBox = () => {
    setShowLight(null);
  };
  return (
    <Fragment>
      <div className="kura_tm_section" id="activity">
        <div className="kura_tm_portfolio" style={{background: '#D0A837'}}>
          <div className="container">
            <div className="kura_tm_main_title">
              <span style={{color:'white'}}> Closer cooperation and collaboration for mutual benefits  </span>
              <h3>紧密合作与互利协作</h3>
            </div>
            <div
              className="portfolio_list gallery_zoom wow fadeInUp"
              data-wow-duration=".7s"
            >
              <div className="swiper-container">
                <div className="swiper-wrapper">
                  <Swiper {...activitySlider} >
                    {IMAGES.map((image, index) => (
                      <SwiperSlide key={index} className="swiper-slide">
                        <div className="list_inner">
                          <div className="image">
                            <img src="/img/portfolio/410-460.jpg" alt="" />
                            <div
                              className="main"
                              style={{
                                backgroundImage: "url(" + image.src + ")",
                              }}
                            ></div>
                            <div className="overlay"></div>

                            <div className="details" style={{paddingRight:'20px'}}>
                              <h3 >{image.title}</h3>
                              {/* <span>{image.sub}</span> */}
                            </div>
                          </div>
                          <a
                            className="kura_tm_full_link popup-vimeo"
                            // onClick={() => console.log(image.link)}
                            href={image.link}
                          ></a>
                        </div>
                      </SwiperSlide>
                    ))}
                  </Swiper>
                </div>
                <div className="kura_tm_swiper_progress fill">
                  <div className="my_pagination_in"></div>
                  <div className="my_navigation">
                    <ul>
                      <li>
                        <a className="my_prev2">
                          <img
                            className="svg"
                            src="img/svg/right-arrow.svg"
                            alt=""
                          />
                        </a>
                      </li>
                      <li>
                        <a className="my_next2">
                          <img
                            className="svg"
                            src="img/svg/right-arrow.svg"
                            alt=""
                          />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {showLight ? (
        <LightBox
          images={IMAGES.map((img) => ({
            url: img.src,
            title: img.caption,
          }))}
          startIndex={showLight.startIndex}
          onClose={hideLightBox}
        />
      ) : null}
    </Fragment>
  );
};

export default Activity;
