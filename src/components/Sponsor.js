import React from "react";
import { Swiper, SwiperSlide } from 'swiper/react';

const Sponsor = () => {
  return (
    <div className="kura_tm_section" id="sponsor">
      <div className="kura_tm_contact" style={{background:'white'}}>
        <div className="container">
          <div className="kura_tm_main_title">
            <span>Thanks to our Sponsors</span>
            <h3>感谢赞助商</h3>
          </div>
          <div className="contact_inner2">
          <Swiper
            loop
            autoplay
            spaceBetween={50}
            slidesPerView={4}
            onSlideChange={() => console.log('slide change')}
            onSwiper={(swiper) => console.log(swiper)}
            breakpoints= {{
              320: {
                slidesPerView: 1,
                spaceBetween: 20,
              },
              640: {
                slidesPerView: 1,
                spaceBetween: 20,
              },
              768: {
                slidesPerView: 2,
                spaceBetween: 30,
              },
              1024: {
                slidesPerView: 3,
                spaceBetween: 40,
              },
            }}
          >
            <SwiperSlide><img src="/img/sponsors/1.JPG" alt="" className='sponsors--image'/></SwiperSlide>
            <SwiperSlide><img src="/img/sponsors/2.JPG" alt="" className='sponsors--image'/></SwiperSlide>
            <SwiperSlide><img src="/img/sponsors/3.JPG" alt="" className='sponsors--image'/></SwiperSlide>
            <SwiperSlide><img src="/img/sponsors/4.JPG" alt="" className='sponsors--image'/></SwiperSlide>
            <SwiperSlide><img src="/img/sponsors/5.JPG" alt="" className='sponsors--image'/></SwiperSlide>
            <SwiperSlide><img src="/img/sponsors/6.JPG" alt="" className='sponsors--image'/></SwiperSlide>
            <SwiperSlide><img src="/img/sponsors/7.PNG" alt="" className='sponsors--image'/></SwiperSlide>



          </Swiper>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Sponsor;
