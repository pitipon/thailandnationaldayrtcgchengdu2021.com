import { Player, PosterImage, BigPlayButton } from 'video-react';

const RightVideo = () => {

  return (
    <div className="kura_tm_section" id="home">
      <div className="kura_tm_hero" style={{paddingTop:'0', background: 'white'}}>
        <div className="container">
          <div className="content">
            <div className="left" >
              <span className="name" style={{fontSize: '25px'}}> Message by Mrs. Thippawan Supamitkitja Consul-General of Thailand in Chengdu</span>
              <h3 className="job" style={{fontSize: '25px'}}>泰国驻成都总领事张淑贞女士致辞</h3>
            </div>
            <div className="right" >

              <Player
                poster="/img/cg-speech.png"
                src="https://image.mfa.go.th/mfa/0/wdW3FTtVMc/5_DEC_2021_Thailand_Day/CG_Speech_29.11.21-new.mp4"
              >
                <BigPlayButton position="center" />
              </Player>

            </div>

          </div>
        </div>
      </div>

    </div>
  );
};

export default RightVideo;
