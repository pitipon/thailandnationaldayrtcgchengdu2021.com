import React, { Fragment, useEffect, useState } from "react";
import { audioSoundAndOpen, openNav, stickyNav } from "../utilits";

const Header = ({ dark }) => {
  const [isToggled, setToggled] = useState(false);
  const toggleTrueFalse = () => {
    setToggled(!isToggled);
    openNav(!isToggled);
  };

  useEffect(() => {
    audioSoundAndOpen();
    window.addEventListener("scroll", stickyNav);
  });

  return (
    <Fragment>
      <div className={"kura_tm_topbar"}>
        <div className="wrapper">
          <div className="topbar_inner">
            <div className="logo">
              <a href="/home">
                <img
                  src="/img/logo/logo.png"
                  alt=""
                />
              </a>
            </div>
            <div className="menu">
              <div className="list">
                <ul className="anchor_nav" id="anchor_nav">
                  <li>
                    <a href="#welcome">Welcome</a>
                  </li>
                  <li>
                    <a href="#video">Video</a>
                  </li>
                  <li>
                    <a href="#quiz">Quiz</a>
                  </li>
                </ul>
              </div>
              <div className="trigger" onClick={() => toggleTrueFalse()}>
                <div
                  className={`hamburger hamburger--slider ${isToggled ? " is-active" : ""
                    }`}
                >
                  <div className="hamburger-box">
                    <div className="hamburger-inner"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="kura_tm_mobile_menu">
        <div className="mobile_menu_inner">
          <div className="mobile_in">
            <div className="logo">
              <a href="/home">
                <img
                  src={"/img/logo/logo.png"}
                  alt=""
                />
              </a>
            </div>
            <div className="trigger" onClick={toggleTrueFalse}>
              <div
                className={
                  isToggled
                    ? "hamburger hamburger--slider is-active"
                    : "hamburger hamburger--slider"
                }
              >
                <div className="hamburger-box">
                  <div className="hamburger-inner"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={isToggled ? "dropdown active" : "dropdown"}>
          <div className="dropdown_inner">
            <ul className="anchor_nav">
              <li className="current">
                <a href="#welcome">Welcome</a>
              </li>
              <li>
                <a href="#video">Video</a>
              </li>
              <li>
                <a href="#quiz">Quiz</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Header;
