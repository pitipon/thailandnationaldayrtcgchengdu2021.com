import React, { Fragment, useEffect, useState } from "react";
import { audioSoundAndOpen, openNav, stickyNav } from "../utilits";

const Header = ({ dark }) => {
  const [isToggled, setToggled] = useState(false);
  const toggleTrueFalse = () => {
    setToggled(!isToggled);
    openNav(!isToggled);
  };

  useEffect(() => {
    audioSoundAndOpen();
    window.addEventListener("scroll", stickyNav);
  });

  return (
    <Fragment>
      <div className={"kura_tm_topbar"} style={{background: 'white', borderBottom:'0px'}}>
        <div className="wrapper">
          <div className="topbar_inner">
            <div className="logo">
              <a href="/">
                <img
                  src="/img/logo/logo.png"
                  alt=""
                />
              </a>
            </div>

          </div>
        </div>
      </div>
      <div className="kura_tm_mobile_menu" >
        <div className="mobile_menu_inner" style={{background: 'white', borderBottom:'0px'}}>
          <div className="mobile_in">
            <div className="logo">
              <a href="/">
                <img
                  src={"/img/logo/logo.png"}
                  alt=""
                />
              </a>
            </div>

          </div>
        </div>

      </div>
    </Fragment>
  );
};

export default Header;
