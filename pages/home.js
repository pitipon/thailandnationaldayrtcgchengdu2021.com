import { Fragment } from "react";
import Audio from "../src/components/Audio";
import Contact from "../src/components/Contact";
import Cursor from "../src/components/Cursor";
import Welcome from "../src/components/Welcome";
import News from "../src/components/News";
import Portfolio from "../src/components/Portfolio";
import Price from "../src/components/Price";
import Skills from "../src/components/Skills";
import Timeline from "../src/components/Timeline";
import Footer from "../src/layout/Footer";
import Header from "../src/layout/Header";
import PageHead from "../src/PageHead";
import Gallery from '../src/components/Gallery'
import Activity from '../src/components/Activity'
import Videos from '../src/components/Videos'
import LeftVideo from '../src/components/LeftVideo'
import RightVideo from '../src/components/RightVideo'
import VideoBox from '../src/components/VideoBox'
import Qrcode from '../src/components/Qrcode'
import Sponsor from '../src/components/Sponsor'
import { Player, PosterImage, BigPlayButton } from 'video-react';
import ReactPlayer from 'react-player';




const Index = () => {
  return (
    <Fragment>

      <PageHead page="Home" />
      <div id="opened">
        <div className="kura_tm_all_wrap" data-color="brown">
          <Header />
          <Welcome />

          {/* <div  className="kura_tm_section" id="video">
            <VideoBox
              url='https://image.mfa.go.th/mfa/0/wdW3FTtVMc/Test/EEC.mp4'
              style={{width:'100vw'}}
            />
          </div> */}

          {/* <div  className="kura_tm_section" id="video2">
            <ReactPlayer url="https://image.mfa.go.th/mfa/0/wdW3FTtVMc/Test/EEC.mp4"  controls/>
          </div> */}



          {/* <div  className="kura_tm_section" id="video2">
            <Player
              poster="/img/test.png"
              src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
            >
            <BigPlayButton position="center" />
            </Player>
          </div> */}


          <div  className="kura_tm_section" id="video">
            <Player
              poster="/img/vtr.png"
              src="https://image.mfa.go.th/mfa/0/wdW3FTtVMc/5_DEC_2021_Thailand_Day/VTR_Thailand_Day_30.11.21-new.mp4"
            >
              <BigPlayButton position="center" />
            </Player>
          </div>

          {/* <LeftVideo/> */}
          <RightVideo/>
          {/* <Gallery /> */}
          <Activity />
          <Videos />
          <Qrcode />
          <Sponsor />
          <Contact />
          <Footer />
          <Audio />
          <Cursor />
        </div>
      </div>
    </Fragment>
  );
};

export default Index;
