import { Fragment, useEffect } from "react";
import { kura_tm_my_load, wowJsAnimation } from "../src/utilits";
import { useAnalytics } from '@happykit/analytics';
import "../styles/globalstyle.css";
function MyApp({ Component, pageProps }) {
  useAnalytics({ publicKey: "analytics_pub_5c393de8c2" })
  useEffect(() => {
    kura_tm_my_load();
    wowJsAnimation();
  });
  return (
    <Fragment>
      <Component {...pageProps} />
    </Fragment>
  );
}

export default MyApp;
