import { Fragment } from "react";
import Banner from "../src/components/Banner";
import PageHead from "../src/PageHead";
import PreLoader from "../src/layout/PreLoader";
import IndexHeader from "../src/layout/IndexHeader";
import Footer from "../src/layout/Footer";
const Index = () => {
  return (
    <Fragment>

      <PageHead page="Index" />
      Coming soon!
    </Fragment>
  );
};

export default Index;
