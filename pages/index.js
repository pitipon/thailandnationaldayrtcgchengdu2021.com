import { Fragment } from "react";
import Banner from "../src/components/Banner";
import PageHead from "../src/PageHead";
import PreLoader from "../src/layout/PreLoader";
import IndexHeader from "../src/layout/IndexHeader2";
import Footer from "../src/layout/Footer";
const Index = () => {
  return (
    <Fragment>
      <PreLoader/>
      <PageHead page="Index" />
      <div id="opened">
        <div className="kura_tm_all_wrap" data-color="brown" style={{background:'white', height:'100vh'}}>
          <IndexHeader />
          <Banner/>
        </div>
      </div>
    </Fragment>
  );
};

export default Index;
